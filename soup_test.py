import requests
from bs4 import BeautifulSoup
import re
import csv

"""
Home work #21
Parsing Lalafo.kg.
Get title, price links of images
"""


def get_html(url):
    r = requests.get(url)
    return r.text


def get_total_pages(html):
    soup = BeautifulSoup(html, 'lxml')
    pages = str(soup.find('li', class_ =  'pagn-last'))
    total_pages = pages.split('?page=')[1].split('\">Последняя ')[0]
    return int(total_pages)


def write_csv(data):
    with open('l.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow((data['price'],
                         data['title'],
                         data['link_to_photo']))


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='mr-3')
    for ad in ads:
        try:
            title = ad.find('a', class_='listing-item-title').text.strip()
        except:
            title =''
        try:
            price = ad.find('p', class_='listing-item-title').text.strip()
        except:
           price = ''
        try:
            link_to_photo = str(ad.find('img', class_='listing-item-photo link-image')).split('\"/>')[0].split(' src=\"')[1]
        except:
            link_to_photo = ''

        data = {'price' : price,
                'title' : title,
                'link_to_photo' : link_to_photo}

        print(data['price'] +'\t' + data['title'] +'\t' + data['link_to_photo'])
        write_csv(data)


def main():
    url = 'https://lalafo.kg/kyrgyzstan/mobilnye-telefony-i-aksessuary/mobilnye-telefony?page='
    total_pages = get_total_pages(get_html(url))
    for i in range(1, 3): #use total_pages
        get_page_data(get_html(url + str(i)))


if __name__ == '__main__':
    main()
